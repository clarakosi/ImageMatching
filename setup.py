from setuptools import setup, find_packages

setup(
    name="algorunner",
    version="2.0.1",
    description="A simple script run the Image Matching algorithm and generate parquet output",
    scripts=['scripts/placeholder_images.py', 'scripts/algorunner.py', 'scripts/algorithm.py'],
    packages=find_packages(),
    package_data={'ima': ['notebooks/*.ipynb', ]},
    install_requires=[
        'beautifulsoup4>=4.9.3',
        'pandas>=1.2.1',
        'papermill>=2.3.2'],
    dependency_links=[
        'wmfdata @ git+ssh://git@github.com/wikimedia/wmfdata-python.git=wmfdata'
    ]
)
